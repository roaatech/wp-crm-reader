<?php

/*
  Plugin Name: SMEPro CRM Integration
  Description: An integration plugin for SME Professional CRM
  Author: Muhannad Shelleh
  Author URI: http://al-shehab.com/
  Version: 1.0
  Text Domain: smecrm-plugin

  This plugin is a property of SME Professional AE.

  All rights reserved, 2015. SME Professional AE

 */

/*
 * Defining constants
 */
error_reporting(E_ALL & ~E_STRICT); // & ~E_NOTICE);
ini_set('show_errors', 1);
ini_set('display_errors', 1);

define('__SMECRM_PLUGIN_FILE__', __FILE__);
define('SMECRM_PLUGIN_PATH', plugin_dir_path(__SMECRM_PLUGIN_FILE__));
define('SMECRM_PLUGIN_EXTERNAL_PATH', plugins_url('SmeCrmPropertiesPlugin'));
define('SMECRM_PLUGIN_IMAGES_PATH', SMECRM_PLUGIN_EXTERNAL_PATH . '/images/');
define('SMECRM_PLUGIN_LOCAL_PATH', __DIR__ . DIRECTORY_SEPARATOR);
define('SMECRM_PLUGIN_VIEWS_LOCAL_PATH', SMECRM_PLUGIN_LOCAL_PATH . 'views' . DIRECTORY_SEPARATOR);

if (!function_exists('dd')) {

    /**
     * dumps variables provided as parameters then dies
     * @param mixed $... list of variables to dump
     */
    function dd() {
        $vars = func_get_args();
        call_user_func_array('var_dump', $vars);
        die();
    }

}

if (!function_exists('array_drop_nulls')) {

    /**
     * 
     * @param array &$array the array to drop its NULL values
     * @return int
     */
    function array_drop_nulls(&$array) {
        $dropped = 0;
        foreach ($array as $key => $value) {
            if ($value === null) {
                unset($array[$key]);
                $dropped++;
            }
        }
        return $dropped;
    }

}

if (!function_exists('array_drop')) {

    /**
     * 
     * @param array &$array the array to drop its NULL values
     * @return int
     */
    function array_drop(&$array, $dropKeys) {
        $dropped = 0;
        foreach ($dropKeys as $key) {
            if (array_key_exists($key, $array)) {
                unset($array[$key]);
                $dropped++;
            }
        }
        return $dropped;
    }

}

/*
 * Methods definition area 
 */

//
if (!function_exists('smecrm_register_post_type')) {

    /**
     * registers the new post type
     */
    function smecrm_register_post_type() {

        register_post_type('property', array(
            'label' => 'Property',
            'has_archive' => true,
            'description' => '',
            'public' => true,
            'show_ui' => true,
            'show_in_menu' => true,
            'capability_type' => 'post',
            'map_meta_cap' => true,
            'hierarchical' => false,
            'rewrite' => array('slug' => 'property', 'with_front' => true),
            'query_var' => true,
            'supports' => array('title', 'editor', 'excerpt', 'trackbacks', 'custom-fields', 'comments', 'revisions', 'thumbnail', 'author', 'page-attributes', 'post-formats'),
            'labels' => array(
                'name' => 'Property',
                'singular_name' => 'Property',
                'menu_name' => 'Property',
                'add_new' => 'Add Property',
                'add_new_item' => 'Add New Property',
                'edit' => 'Edit',
                'edit_item' => 'Edit Property',
                'new_item' => 'New Property',
                'view' => 'View Property',
                'view_item' => 'View Property',
                'search_items' => 'Search Property',
                'not_found' => 'No Property Found',
                'not_found_in_trash' => 'No Property Found in Trash',
                'parent' => 'Parent Property',
            )
        ));
    }

}

if (!function_exists('smecrm_adminmenu')) {

    /**
     * Adds admin menu
     */
    function smecrm_adminmenu() {
        add_menu_page('smecrm', 'SME Integrator', 7, SMECRM_PLUGIN_PATH, 'smecrm_admin_settings', SMECRM_PLUGIN_IMAGES_PATH . "visualization.png");
    }

}

if (!function_exists('smecrm_refresh_properties')) {

    /**
     * Initiates and calls the refresh method in the CRM Integrator class
     */
    function smecrm_refresh_properties() {

        //require the file
        require_once __DIR__ . DIRECTORY_SEPARATOR . "CrmWpIntegrator.php";

        //initate it
        $module = new CrmWpIntegrator();

        //and call it
        $module->refresh();
    }

}

if (!function_exists('smecrm_properties_meta')) {

    /**
     * Returns a JSON string of meta keys for search
     */
    function smecrm_properties_meta() {
        global $wpdb;
        $meta = $wpdb->get_results("SELECT meta_key as `key`, meta_value as `value`, count(wp_posts.id) as `cnt` from wp_postmeta join wp_posts on wp_posts.ID = post_id where meta_key in ('city','community','subcommunity','bedrooms','bathrooms','contractType','propertyType') group by meta_key, meta_value", 'ARRAY_A');
        $results = array();
        $last = null;
        foreach ($meta as $record) {
            if ($record['key'] !== $last) {
                $last = $record['key'];
                $results[$last] = array();
            }
            $results[$last][$record['value']] = $record['cnt'];
        }
        header('Content-Type:application/json;');
        echo json_encode($results);
        exit();
    }

}

if (!function_exists('smecrm_admin_settings')) {

    /**
     * Adds the admin interface when called
     * 
     * @return boolean
     */
    function smecrm_admin_settings() {
        include SMECRM_PLUGIN_VIEWS_LOCAL_PATH . 'admin-interface.php';
        return false;
    }

}

if (!function_exists('smecrm_admin_property_post_metabox')) {

    /**
     * Adds the metabox on call
     */
    function smecrm_admin_property_post_metabox() {
        add_meta_box('property_meta_box', 'Property Details', 'smecrm_display_admin_property_post_metabox', 'property', 'normal', 'high');
    }

}

if (!function_exists('smecrm_display_admin_property_post_metabox')) {

    /**
     * Displays the metabox in the property post page in admin interfaces
     * 
     * @param type $propertyPost
     */
    function smecrm_display_admin_property_post_metabox($propertyPost) {
        include SMECRM_PLUGIN_VIEWS_LOCAL_PATH . 'admin-property-post-metabox.php';
    }

}

if (!function_exists('smecrm_add_property_post_metadata_values')) {

    function smecrm_add_property_post_metadata_values($samplepost_id, $samplepost) {
        if ($samplepost->post_type == 'property') {
            if (isset($_POST['meta'])) {
                foreach ($_POST['meta'] as $key => $value) {
                    update_post_meta($samplepost_id, $key, $value);
                }
            }
        }
    }

}

if (!function_exists('smecrm_filter_properties')) {

    function smecrm_filter_properties($query) {

        $filters = array();

        if (isset($_GET['city'])) {
            $filters[] = array('key' => 'city', 'value' => $_GET['city']);
        }

        if (isset($_GET['bathrooms'])) {
            $filters[] = array('key' => 'bathrooms', 'value' => $_GET['bathrooms']);
        }

        if (isset($_GET['bedrooms'])) {
            $filters[] = array('key' => 'bedrooms', 'value' => $_GET['bedrooms']);
        }

        if (isset($_GET['community'])) {
            $filters[] = array('key' => 'community', 'value' => $_GET['community']);
        }

        if (isset($_GET['contractType'])) {
            $filters[] = array('key' => 'contractType', 'value' => $_GET['contractType']);
        }

        if (isset($_GET['price'])) {
            $filters[] = array('key' => 'price', 'value' => $_GET['price']);
        }

        if (isset($_GET['propertyType'])) {
            $filters[] = array('key' => 'propertyType', 'value' => $_GET['propertyType']);
        }

        if (isset($_GET['agentName'])) {
            $filters[] = array('key' => 'agentName', 'value' => $_GET['agentName']);
        }

        if (isset($_GET['subcommunity'])) {
            $filters[] = array('key' => 'subcommunity', 'value' => $_GET['subcommunity']);
        }

        // Check this is main query and other conditionals as needed
        if ($query->is_main_query()) {
            $query->set('meta_query', $filters);
        }
    }

}

/*
 * Filters and actions area
 */

// when the specific URL is called, invoke the refresh method. It should include: ?module=smecrm&properties=refresh
if (isset($_GET['module']) && $_GET['module'] == 'smecrm' && isset($_GET['properties']) && strtolower($_GET['properties']) == 'refresh') {
    add_action("wp_loaded", "smecrm_refresh_properties", 10);
}
//meta data as json
if (isset($_GET['module']) && $_GET['module'] == 'smecrm' && isset($_GET['properties']) && strtolower($_GET['properties']) == 'meta') {
    add_action("init", "smecrm_properties_meta");
}
// sets the CRM XML URL on post
if (isset($_POST['smecrm_xml_url'])) {
    update_option('smecrm_xml_url', $_POST['smecrm_xml_url']);
}
if (isset($_POST['smecrm_xml_url_mk'])) {
    $url = @$_POST['smecrm_xml_url_mk'];
    $access = @$_POST['smecrm_xml_url_mk_access_code'];
    $group = @$_POST['smecrm_xml_url_mk_group_code'];
    update_option('smecrm_xml_url_mk', $url);
    update_option('smecrm_xml_url_mk_access_code', $access);
    update_option('smecrm_xml_url_mk_group_code', $group);
}
//add the admin menu
add_action('admin_menu', 'smecrm_adminmenu', 1);
//add the post metadata
add_action('admin_init', 'smecrm_admin_property_post_metabox');
//add metadata values on save
add_action('save_post', 'smecrm_add_property_post_metadata_values', 10, 2);
//register the type
add_action('init', 'smecrm_register_post_type');
//filter for properties listing
if (isset($_GET['post_type']) && $_GET['post_type'] == 'property') {
    add_action('pre_get_posts', 'smecrm_filter_properties');
}