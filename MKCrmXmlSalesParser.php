<?php

class MKCrmXmlSalesParser extends MKXmlParser {

    public static function make($url, $accessCode, $groupCode, $others = array()) {
        $others += array(
            "Bedrooms" => "",
            "StartPriceRange" => "",
            "EndPriceRange" => "",
            "CategoryID" => "",
            "SpecialProjects" => "",
            "CountryID" => "",
            "StateID" => "",
            "CommunityID" => "",
            "DistrictID" => "",
            "FloorAreaMin" => "",
            "FloorAreaMax" => "",
            "UnitCategory" => "",
            "UnitID" => "",
            "BedroomsMax" => "",
            "PropertyID" => "",
            "ReadyNow" => "",
            "PageIndex" => ""
        );
        return parent::make($url . "SalesListings", $accessCode, $groupCode, $others);
    }

    protected function xmlToProperty(SimpleXMLElement $xml) {
        return array('contractType' => (string) "Sale {$this->getType((string) $xml->Category)}",
            'price' => (float) $xml->SellPrice) + $this->_xmlToProperty($xml);
    }

}
