# What is this
A Wordpress module to integrate SME Professional CRM with a Wordpress blog

## There are two classes: 
1. SMEPro_XMLParser which is not related to WP directly and it handles the logic of reading, parsing, and extracting the properties from the XML content
2. SMEPro_CRM_WP_Module which will be completed by the WP developer.

## WP Developer
The developer should search for 
//TODO: WP developer
and read the comment, implement the method, and commit it back.

Also, there the developer should develop the other traditional parts of WP to search, get, and list special post type contents as described previously.