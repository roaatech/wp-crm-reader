<?php

class MKCrmXmlRentsParser extends MKXmlParser {

    public static function make($url, $accessCode, $groupCode, $others = array()) {
        $others+=array(
            "PropertyType" => "",
            "Bedrooms" => "",
            "StartPriceRange" => "",
            "EndPriceRange" => "",
            "categoryID" => "",
            "CountryID" => "",
            "StateID" => "",
            "CommunityID" => "",
            "FloorAreaMin" => "",
            "FloorAreaMax" => "",
            "UnitCategory" => "",
            "UnitID" => "",
            "BedroomsMax" => "",
            "PropertyID" => "",
            "ReadyNow" => "",
            "PageIndex" => ""
        );
        return parent::make($url . "RentListings", $accessCode, $groupCode, $others);
    }

    protected function xmlToProperty(SimpleXMLElement $xml) {
        return array('contractType' => (string) "Rent {$this->getType((string) $xml->Category)}",
            'price' => (float) $xml->Rent) + $this->_xmlToProperty($xml);
    }

}
