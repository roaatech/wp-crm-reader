<?php

/**
 * CRM XML Feed Reader
 * 
 * It reads CRM XML feed
 * 
 * @author Muhannad Shelleh
 * 
 */
class CrmWpIntegrator {

    static $results = "";
    static $inserted = 0;
    static $Updated = 0;
    static $deleted = 0;
    static $kept = 0;

    /**
     * This is the main update method to be used to update the properties with the ones from the CRM.
     * 
     * This method is to be called when the URL's query string contains: module=mkcrm&properties=refresh
     */
    public function refresh() {

        //sets the time limit to unlimited
        set_time_limit(0);

        //Getting the content
        $fresh = $this->getRefreshedProperties();
        $current = $this->getWPProperties();

        $results = "";

        //Add, update, delete loops
        foreach ($fresh as $property) {//loop: add or update
            $internalId = $property['internalId']; //get the internal ID from the property

            if (array_key_exists($internalId, $current)) {//exists in current?
                if ($current[$internalId]['updated'] < $property['updated']) {//not up to date?
                    $this->updateWPProperty($current[$internalId]['ID'], $property); //update

                    $results .= "Update property {$property['internalId']}<br />\n";
                    static::$updated++;
                } else {
                    //alread updated 
                    $results .= "Property {$property['internalId']} is up to date<br />\n";
                    static::$kept++;
                }

                unset($current[$internalId]); //remove from current list as it has been processed
            } else {//not in the current list?
                $this->addWPProperty($property); //add it

                $results .= "Add property {$property['internalId']}<br />\n";
                static::$inserted++;
            }
        }

        foreach ($current as $internalId => $property) {//loop: delete
            $this->removeWPProperty($property['ID']); //remove it

            $results .= "Delete property {$internalId}<br />\n";
            static::$deleted++;
        }

        static::$results = $results;
    }

    /**
     * Get list of references for active properties with last update timestamp
     * 
     * A sample return can be:
     * [
     *  'rent-02988982' => ['ID' => 1325, 'updated' => 13215658989],
     *  'dxb-9398-sell' => ['ID' => 5652, 'updated' => 13215658989],
     *  'some-ref-2991' => ['ID' => 9221, 'updated' => 13215658989],
     * ]
     * 
     * @return array A two dim array where keys are internal ids, values are assoc array with two elements: wpId (WP post ID) and updated (updated timestamp from create data)
     */
    protected function getWPProperties() {

        global $wpdb;
        /* @var $wpdb wpdb */

        $prefix = $wpdb->base_prefix;

        $query = "SELECT * FROM `{$prefix}posts` WHERE `post_type`='property' and `post_status`='publish'";

        //get the posts
        $posts = $wpdb->get_results($query);


        //prepare the result container
        $result = array();

        //loop
        foreach ($posts as $post) {
            $internalId = get_post_meta($post->ID, "internalId", 1);
            $updated = get_post_meta($post->ID, "updated", 1);
            $result[$internalId] = array("ID" => $post->ID, "updated" => $updated);
        }

        return $result;
    }

    /**
     * Unpublish the referenced post ID
     * @param integer $postId the post ID to take it down
     */
    protected function removeWPProperty($postId) {
        $post = wp_delete_post($postId, true);
        return $post;
    }

    /**
     * Creates a new property post with the provided data.
     * 
     * The keys and values of the $data array are as follows:
     * [
      'title' => string: the title
      'description' => text: the content
      'referenceID' => string: a unique reference code which is the ID of the property in the other system
      'bedrooms' => int: number of bedrooms
      'bathrooms' => int: number of bedrooms
      'floor' => string: the floor in which this property in. May be null
      'area' => int: a total area of the property
      'updated' => int: timestamp used to compare for updates. Just keep and return as is.
      'price' => float: the price
      'priceSuffix' => string: a string representing the type: annual, monthly, .. may be null
      'amenities' => array: Am array of strings representing different amenities of the property.
      'contractType' => string: Rent/Sale/...
      'propertyType' => string: Villa, Office, Apartment, ...
      'city' => string: A city like Dubai, Sharjah, ...
      'community' => string: A community in the city. i.e. Business Bay, Airport, ...
      'subcommunity' => string: A subcommunity in the community. i.e. Executive towers, ...
      'tower' => string: a tower in which the property exists. may be null
      'latitude' => float: geolat
      'longitude' => float: geolong
      'agentReference' => string: reference of the agent. maybe null
      'featuredProperty' => bool: true/false
      'images' => array: a list of URLs to different images to the property. Save and return as an array
     * ]
     * 
     * @param array $data the data to be used as property post content and attributes
     */
    protected function addWPProperty(array $data) {

        $newPost = array(
            'post_title' => wp_strip_all_tags($data["title"]),
            'post_type' => 'property',
            'post_content' => sanitize_text_field($data["description"]),
            'post_status' => 'publish'
        );

        array_drop($data, array('title', 'description'));

        $postId = wp_insert_post($newPost);

        foreach ($data as $key => $value) {
            update_post_meta($postId, $key, $value);
        }

        return true;
    }

    /**
     * Updates a currently posted and published property post.
     * 
     * The structure of the $data array is as described in addWPProperty method
     * 
     * @param integer $postId the post ID to be updated
     * @param array $data the data to be used as property post content and attributes
     */
    protected function updateWPProperty($postId, array $data) {

        $data['ID'] = $postId;

        $result = wp_update_post($data);

        if (!$result) {
            return false;
        }

        array_drop($data, array('title', 'description'));

        foreach ($data as $key => $value) {
            update_post_meta($postId, $key, $value);
        }

        return true;
    }

    /**
     * Gets the info to access MK CRM
     * 
     * @return array [url, accessCode, groupCode]
     */
    protected function getMkCrmInfo() {
        // i.e.: return ["http://api.gomasterkey.com/v1.2/website.asmx/", "4e92101214", "1993"];
        $url = get_option("smecrm_xml_url_mk");
        return $url ? array($url, get_option("smecrm_xml_url_mk_access_code"), get_option("smecrm_xml_url_mk_group_code")) : false;
    }

    /**
     * Gets the info to access SME CRM
     * 
     * @return array [url, null, null]
     */
    protected function getSmeCrmInfo() {
        // i.e.: return ["https://crm.smeprofessional.ae/xmlFeed.php?clid=40202&accid=18"];
        $url = get_option("smecrm_xml_url");
        return $url ? array($url) : false;
    }

    /**
     * Initates and invoke an object of class SMEPro_XMLParser with the URL
     * 
     * Calls the URL, parses the XML, converts it to array, and returns it
     * 
     * @return array
     */
    protected function getRefreshedProperties() {
        require_once(__DIR__ . DIRECTORY_SEPARATOR . 'XmlParser.php');

        $resultsArrays = [];

        $smeInfo = $this->getSmeCrmInfo();
        if ($smeInfo) {
            require_once(__DIR__ . DIRECTORY_SEPARATOR . 'SmeCrmXmlParser.php');
            $resultsArrays[] = SmeCrmXmlParser::make($smeInfo[0])->read()->parse()->properties();
        }

        $mkInfo = $this->getMkCrmInfo();
        if ($mkInfo) {
            require_once(__DIR__ . DIRECTORY_SEPARATOR . 'MKXmlParser.php');
            require_once(__DIR__ . DIRECTORY_SEPARATOR . 'MKCrmXmlRentsParser.php');
            require_once(__DIR__ . DIRECTORY_SEPARATOR . 'MKCrmXmlSalesParser.php');
            $resultsArrays[] = MKCrmXmlRentsParser::make($mkInfo[0], $mkInfo[1], $mkInfo[2])->read()->parse()->properties();
            $resultsArrays[] = MKCrmXmlSalesParser::make($mkInfo[0], $mkInfo[1], $mkInfo[2])->read()->parse()->properties();
        }

        if (count($resultsArrays) == 0) {
            echo "<p><strong>Error!</strong> No URLs defined!<br /></p>";
        }

        $properties = call_user_func_array('array_merge', $resultsArrays);
        return $properties;
    }

}
