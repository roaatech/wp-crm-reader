<?php

/**
 * Description of MkXmlParser
 *
 * @author muhannad
 */
abstract class MKXmlParser extends XmlParser {

    /**
     * Creates a new instance
     * 
     * @param string $url
     * @return MKCrmXmlParser
     */
    public static function make($url, $accessCode, $groupCode, array $others = []) {
        $others += array('AccessCode' => $accessCode, 'GroupCode' => $groupCode);
        $url .= "?" . http_build_query($others);
        return new static($url);
    }

    protected function shouldContinue(\SimpleXMLElement $entry) {
        return (string) $entry->Status === 'Vacant';
    }

    protected function getList(\SimpleXMLElement $doc) {
        return $doc->UnitDTO;
    }

    protected function getSource() {
        return 'MK';
    }

    protected function _xmlToProperty(SimpleXMLElement $xml) {
        $result = array(
            'title' => (string) $xml->MarketingTitle,
            'description' => (string) $xml->Remarks,
            'referenceID' => (string) $xml->RefNo,
            'bedrooms' => (int) $xml->Bedrooms,
            'bathrooms' => (int) $xml->NoOfBathrooms,
            'floor' => (string) $xml->FloorNo,
            'area' => (int) $xml->BuiltupArea,
            'listed' => (integer) strtotime($xml->ListingDate),
            'updated' => (integer) strtotime($xml->LastUpdated),
            'priceSuffix' => (string) 'Annual',
            'amenities' => trim(trim(join("\n", $xml->FittingFixtures && $xml->FittingFixtures->FittingFixture ? $this->xmlFixturesToArray($xml->FittingFixtures) : array()), "\n")),
            'propertyType' => (string) $xml->Category,
            'city' => (string) $xml->CityName,
            'community' => (string) $xml->Community,
            'agentName' => (string) $xml->Agent,
            'subcommunity' => (string) $xml->SubCommunity,
            'tower' => (string) $xml->PropertyName,
            'latitude' => (float) 0, //$xml->ProGoogleCoordinates are empty
            'longitude' => (float) 0, //$xml->ProGoogleCoordinates are empty
            'agentReference' => (string) $xml->AgentID,
            'featuredProperty' => (bool) false, //no $xml-> for featured
            'images' => trim(trim(join("\n", $xml->Images && $xml->Images->Image ? $this->xmlImagesToArray($xml->Images) : array()), "\n")),
        );
        return $result;
    }

    protected function getType($category) {
        switch ($category) {
            case 'Warehouse':
            case 'Retail':
            case 'Office':
            case 'Commercial Villa':
            case 'Commercial Plot':
            case 'Commercial Building':
                return 'Commercial';
            default:
                return 'Residential';
        }
    }

    protected function xmlFixturesToArray(SimpleXMLElement $xml) {
        $result = array();
        foreach ($xml->FittingFixture as $item) {
            $result[] = (string) $item->Name;
        }
        return $result;
    }

    protected function xmlImagesToArray(SimpleXMLElement $xml) {
        $result = array();
        foreach ($xml->Image as $item) {
            $result[] = (string) $item->ImageURL;
        }
        return $result;
    }

}
