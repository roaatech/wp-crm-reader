<div class="wrap couponcode"> 
    <div class="wrap">
        <h2 class="settings"><em><?php _e('Properties CRM Integrations') ?></em></h2>	
        <form name="form1" method="post" action="">
            <?php //wp_nonce_field('CC_validate_nonce', 'couponcode_nonce');         ?>
            <table width="100%" cellspacing="2" cellpadding="5" class="widefat" style="margin-top: 1em;"> 
                <thead>
                    <tr>
                        <th colspan="2">SME Professional CRM</th>
                    </tr>
                </thead>
                <tbody>
                    <tr valign="top"> 
                        <th scope="row" class='row-title' style="width:33%;"><?php _e('XML URL') ?>: </th> 
                        <td class='desc'>
                            <input type="url" name="smecrm_xml_url" value="<?php echo get_option("smecrm_xml_url"); ?>" maxlength="255" style="width: 50%;" />
                        </td> 
                    </tr>
                </tbody>
                <tfoot>
                    <tr>
                        <td></td>
                        <td>
                            <input type="submit" name="smecrm_save_settings" value="Save" />
                        </td>
                    </tr>
                </tfoot>
            </table>
        </form>
        <form name="form1" method="post" action="">
            <table width="100%" cellspacing="2" cellpadding="5" class="widefat" style="margin-top: 1em;"> 
                <thead>
                    <tr>
                        <th colspan="2">MasterKey CRM</th>
                    </tr>
                </thead>
                <tbody>
                    <tr valign="top"> 
                        <th scope="row" class='row-title' style="width:33%;"><?php _e('XML URL') ?>: </th> 
                        <td class='desc'>
                            <input type="url" name="smecrm_xml_url_mk" value="<?php echo get_option("smecrm_xml_url_mk"); ?>" maxlength="255" style="width: 50%;" />
                        </td> 
                    </tr>
                    <tr valign="top"> 
                        <th scope="row" class='row-title'><?php _e('Access Code') ?>: </th> 
                        <td class='desc'>
                            <input type="text" name="smecrm_xml_url_mk_access_code" value="<?php echo get_option("smecrm_xml_url_mk_access_code"); ?>" maxlength="255"/>
                        </td> 
                    </tr>
                    <tr valign="top"> 
                        <th scope="row" class='row-title'><?php _e('Group Code') ?>: </th> 
                        <td class='desc'>
                            <input type="text" name="smecrm_xml_url_mk_group_code" value="<?php echo get_option("smecrm_xml_url_mk_group_code"); ?>" maxlength="255"/>
                        </td> 
                    </tr>
                </tbody>
                <tfoot>
                    <tr>
                        <td></td>
                        <td>
                            <input type="submit" name="smecrm_save_settings" value="Save" />
                        </td>
                    </tr>
                </tfoot>
            </table>
        </form>
    </div>
    <div style="padding: 20px 0px;">
        <strong>
            Update properties from CRM: 
        </strong>
        <a href="<?= add_query_arg(array('properties' => 'refresh', 'module' => 'smecrm', 'what' => 'MK')) ?>">Refresh</a>!
    </div>
    <?php require_once(__DIR__ . '/../CrmWpIntegrator.php'); ?>
    <?php if (CrmWpIntegrator::$results): ?>
        <hr />
        <h3>Refresh results:</h3>
        <strong>New:</strong> <?= CrmWpIntegrator::$inserted ?>.<br />
        <strong>Updated:</strong> <?= CrmWpIntegrator::$Updated ?>.<br />
        <strong>Deleted:</strong> <?= CrmWpIntegrator::$deleted ?>.<br />
        <strong>Kept:</strong> <?= CrmWpIntegrator::$kept ?>.<br />
        <hr />
        <?= CrmWpIntegrator::$results ?>
    <?php endif; ?>
</div>