<h4>General Details</h4>
<table width="100%">
    <tr>
        <td style="width: 25%">Source CRM</td>
        <td>
            <label style="margin-right: 25px;">
                <input type="radio" name="meta[sourceCrm]" value="SME" <?= esc_html(get_post_meta($propertyPost->ID, 'sourceCrm', true))=='SME'?'checked="checked"':''; ?> />
                SME
            </label>
            <label style="margin-right: 25px;">
                <input type="radio" name="meta[sourceCrm]" value="MK" <?= esc_html(get_post_meta($propertyPost->ID, 'sourceCrm', true))=='MK'?'checked="checked"':''; ?> />
                MasterKey
            </label>
        </td>
    </tr>
    <tr>
        <td style="width: 25%">internal Id</td>
        <td>
            <input type="hidden" style="width: 50%;" name="meta[internalId]" value="<?php echo esc_html(get_post_meta($propertyPost->ID, 'internalId', true)); ?>" />
            <?= esc_html(get_post_meta($propertyPost->ID, 'internalId', true)) ?>
        </td>
    </tr>
    <tr>
        <td style="width: 25%">ReferenceID</td>
        <td><input type="text" style="width: 50%;" name="meta[referenceID]" value="<?php echo esc_html(get_post_meta($propertyPost->ID, 'referenceID', true)); ?>" />
        </td>
    </tr>
    <tr>
        <td>Bedrooms</td>
        <td><input type="text" style="width: 50%;" name="meta[bedrooms]" value="<?php echo esc_html(get_post_meta($propertyPost->ID, 'bedrooms', true)); ?>" />
        </td>
    </tr>
    <tr>
        <td>Bathrooms</td>
        <td><input type="text" style="width: 50%;" name="meta[bathrooms]" value="<?php echo esc_html(get_post_meta($propertyPost->ID, 'bathrooms', true)); ?>" />
        </td>
    </tr>
    <tr>
        <td>Floor</td>
        <td><input type="text" style="width: 50%;" name="meta[floor]" value="<?php echo esc_html(get_post_meta($propertyPost->ID, 'floor', true)); ?>" />
        </td>
    </tr>
    <tr>
        <td>Area</td>
        <td><input type="text" style="width: 50%;" name="meta[area]" value="<?php echo esc_html(get_post_meta($propertyPost->ID, 'area', true)); ?>" />
        </td>
    </tr>
    <tr>
        <td>Updated</td>
        <td><input type="text" style="width: 50%;" name="meta[updated]" value="<?php echo esc_html(get_post_meta($propertyPost->ID, 'updated', true)); ?>" />
        </td>
    </tr>
    <tr>
        <td>Price</td>
        <td><input type="text" style="width: 50%;" name="meta[price]" value="<?php echo esc_html(get_post_meta($propertyPost->ID, 'price', true)); ?>" />
        </td>
    </tr>
    <tr>
        <td>PriceSuffix</td>
        <td><input type="text" style="width: 50%;" name="meta[priceSuffix]" value="<?php echo esc_html(get_post_meta($propertyPost->ID, 'priceSuffix', true)); ?>" />
        </td>
    </tr>
    <tr>
        <td>Amenities</td>
        <td>
            <textarea style="width: 50%;" name="meta[amenities]"><?php echo esc_html(get_post_meta($propertyPost->ID, 'amenities', true)); ?></textarea>
        </td>
    </tr>
    <tr>
        <td>ContractType</td>
        <td><input type="text" style="width: 50%;" name="meta[contractType]" value="<?php echo esc_html(get_post_meta($propertyPost->ID, 'contractType', true)); ?>" />
        </td>
    </tr>
    <tr>
        <td>PropertyType</td>
        <td><input type="text" style="width: 50%;" name="meta[propertyType]" value="<?php echo esc_html(get_post_meta($propertyPost->ID, 'propertyType', true)); ?>" />
        </td>
    </tr>
    <tr>
        <td>City</td>
        <td><input type="text" style="width: 50%;" name="meta[city]" value="<?php echo esc_html(get_post_meta($propertyPost->ID, 'city', true)); ?>" />
        </td>
    </tr>
    <tr>
        <td>Community</td>
        <td><input type="text" style="width: 50%;" name="meta[community]" value="<?php echo esc_html(get_post_meta($propertyPost->ID, 'community', true)); ?>" />
        </td>
    </tr>
    <tr>
        <td>SubCommunity</td>
        <td><input type="text" style="width: 50%;" name="meta[subcommunity]" value="<?php echo esc_html(get_post_meta($propertyPost->ID, 'subcommunity', true)); ?>" />
        </td>
    </tr>
    <tr>
        <td>Tower</td>
        <td><input type="text" style="width: 50%;" name="meta[tower]" value="<?php echo esc_html(get_post_meta($propertyPost->ID, 'tower', true)); ?>" />
        </td>
    </tr>
    <tr>
        <td>Latitude</td>
        <td><input type="text" style="width: 50%;" name="meta[latitude]" value="<?php echo esc_html(get_post_meta($propertyPost->ID, 'latitude', true)); ?>" />
        </td>
    </tr>
    <tr>
        <td>Longitude</td>
        <td><input type="text" style="width: 50%;" name="meta[longitude]" value="<?php echo esc_html(get_post_meta($propertyPost->ID, 'longitude', true)); ?>" />
        </td>
    </tr>
    <tr>
        <td>AgentReference</td>
        <td><input type="text" style="width: 50%;" name="meta[agentReference]" value="<?php echo esc_html(get_post_meta($propertyPost->ID, 'agentReference', true)); ?>" />
        </td>
    </tr>
    <tr>
        <td>FeaturedProperty</td>
        <td><input type="text" style="width: 50%;" name="meta[featuredProperty]" value="<?php echo esc_html(get_post_meta($propertyPost->ID, 'featuredProperty', true)); ?>" />
        </td>
    </tr>
    <tr>
        <td>Images</td>
        <td>
            <textarea style="width: 50%;" name="meta[images]"><?php echo get_post_meta($propertyPost->ID, 'images', true); ?></textarea>
        </td>
    </tr>


</table>