<?php

if (!function_exists('xml2array')) {

    /**
     * function xml2array
     *
     * This function is part of the PHP manual.
     *
     * The PHP manual text and comments are covered by the Creative Commons 
     * Attribution 3.0 License, copyright (c) the PHP Documentation Group
     *
     * @author  k dot antczak at livedata dot pl
     * @date    2011-04-22 06:08 UTC
     * @link    http://www.php.net/manual/en/ref.simplexml.php#103617
     * @license http://www.php.net/license/index.php#doc-lic
     * @license http://creativecommons.org/licenses/by/3.0/
     * @license CC-BY-3.0 <http://spdx.org/licenses/CC-BY-3.0>
     */
    function xml2array($xmlObject, $out = array()) {
        foreach ((array) $xmlObject as $index => $node)
            $out[$index] = ( is_object($node) ) ? xml2array($node) : $node;

        return $out;
    }

}

/**
 * Description of XmlParser
 *
 * @author muhannad
 */
abstract class XmlParser {

    protected $url;
    protected $content;
    protected $properties;
    protected $agents;

    /**
     * Creates a new instance
     * 
     * @param string $url
     * @return SmeCrmXmlParser
     */
    public static function make($url) {
        return new static($url);
    }

    /**
     * 
     * @param string $url the CRM XML URL
     */
    public function __construct($url) {
        $this->url = $url;
    }

    public function read() {
        $this->content = file_get_contents($this->url);
        return $this;
    }

    abstract protected function xmlToProperty(SimpleXMLElement $xml);

    abstract protected function getList(SimpleXMLElement $doc);

    protected function shouldContinue(SimpleXMLElement $entry) {
        return true;
    }

    abstract protected function getSource();

    public function parse() {
        $root = new SimpleXMLElement($this->content);
        $entries = $this->getList($root);
        $properties = array();
        foreach ($entries as $entry) {

            if (!$this->shouldContinue($entry)) {
                continue;
            }

            $property = $this->xmlToProperty($entry);

            $property['images'] = trim(trim($property['images'], "\n"));
            $property['amenities'] = trim(trim($property['amenities'], "\n"));
            $property['sourceCrm'] = $this->getSource();
            $property['internalId'] = strtolower(trim(str_replace(' ', '_', join('-', array(
                $property['sourceCrm'],
                $property['referenceID'] ? $property['referenceID'] : ($property['listed'] ? $property['listed'] : '0'),
                $property['price'] ? $property['price'] : '0',
                $property['city'] ? $property['city'] : 'no-city',
                $property['contractType'] ? $property['contractType'] : 'contract-type',
                $property['community'] ? $property['community'] : 'community',
                $property['subcommunity'] ? $property['subcommunity'] : 'subcommunity',
                ($property['bedrooms'] ? $property['bedrooms'] : '0') . "br",
                $property['images'] ? count(explode("\n", $property['images'])) : '0',
                $property['amenities'] ? count(explode("\n", $property['amenities'])) : '0',
            )))));

            $properties[] = $property;
        }

        $this->properties = $properties;

        return $this;
    }

    public function properties() {
        return $this->properties;
    }

}
