<?php

class SmeCrmXmlParser extends XmlParser {

    protected function getList(\SimpleXMLElement $doc) {
        return $doc->properties->property;
    }

    protected function getSource() {
        return 'SME';
    }

    protected function xmlToArray(SimpleXMLElement $xml) {
        $result = array();
        foreach ($xml as $item) {
            $result[] = (string) $item;
        }
        return $result;
    }

    protected function xmlToProperty(\SimpleXMLElement $xml) {
        $result = array(
            'title' => (string) $xml->title,
            'description' => (string) $xml->description,
            'referenceID' => (string) $xml->referenceID,
            'bedrooms' => (int) $xml->bedrooms,
            'bathrooms' => (int) $xml->bathrooms,
            'floor' => (string) $xml->floor,
            'area' => (int) $xml->area,
            'listed' => (integer) $xml->listing_date,
            'updated' => (integer) $xml->updated,
            'price' => (float) $xml->price,
            'priceSuffix' => (string) $xml->price->attributes()['suffix'],
            'amenities' => trim(trim(join("\n", $xml->amenities ? ($xml->amenities->item ? $this->xmlToArray($xml->amenities->item) : (string) $xml->amenities) : array()), "\n")),
            'propertyType' => (string) $xml->propertyType,
            'contractType' => (string) $xml->contractType,
            'city' => (string) $xml->city,
            'community' => (string) $xml->community,
            'agentName' => (string) $xml->agentName,
            'subcommunity' => (string) $xml->subcommunity,
            'tower' => (string) $xml->tower,
            'latitude' => (float) $xml->latitude,
            'longitude' => (float) $xml->longitude,
            'agentReference' => (string) $xml->agentReference,
            'featuredProperty' => (bool) $xml->featuredProperty,
            'images' => trim(trim(join("\n", $xml->images ? ($xml->images->item ? $this->xmlToArray($xml->images->item) : (string) $xml->images) : array()), "\n")),
        );

        return $result;
    }

}
